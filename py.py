import requests
from bs4 import BeautifulSoup

res = requests.get('https://www.airbnb.com.tw/s/honolulu/homes?tab_id=home_tab&refinement_paths%5B%5D=%2Fhomes&flexible_trip_dates%5B%5D=june&flexible_trip_dates%5B%5D=may&flexible_trip_lengths%5B%5D=weekend_trip&date_picker_type=flexible_dates&adults=1&source=structured_search_input_header&search_type=user_map_move&query=Honolulu%2C%20HI%2C%20United%20States&ne_lat=21.419829159689478&ne_lng=-157.64860406755656&sw_lat=21.12165069539393&sw_lng=-158.02607599417865&zoom=11&search_by_map=true')
soup = BeautifulSoup(res.text, 'lxml')

f = open("demo.txt", "w")
f.write(soup.prettify())
f.close()
